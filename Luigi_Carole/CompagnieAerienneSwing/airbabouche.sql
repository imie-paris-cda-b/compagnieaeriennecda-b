-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 18 jan. 2021 à 23:51
-- Version du serveur :  10.4.17-MariaDB
-- Version de PHP : 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `airbabouche`
--

-- --------------------------------------------------------

--
-- Structure de la table `aeroport`
--

CREATE TABLE `aeroport` (
  `idaeroport` varchar(5) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `ville` varchar(70) NOT NULL,
  `pays` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `aeroport`
--

INSERT INTO `aeroport` (`idaeroport`, `nom`, `ville`, `pays`) VALUES
('ALG', 'Alger-Houari-Boumédiène', 'Alger', 7),
('CDG', 'Charles de Gaulle', 'Roissy', 1),
('FCO', 'Aeroporto Leonardo da Vinci di Roma', 'Rome', 8),
('FRA', 'Flughafen Frankfurt am Main', 'Francfort', 2),
('LHR', 'London Heathrow Airport', 'Londres', 6),
('ORY', 'ORLY', 'Orly', 1);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `email` varchar(200) NOT NULL,
  `password` varchar(80) NOT NULL,
  `personne_idpersonne` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`email`, `password`, `personne_idpersonne`) VALUES
('luigi.carole@gmail.com', 'xyz', 2);

-- --------------------------------------------------------

--
-- Structure de la table `compagnie`
--

CREATE TABLE `compagnie` (
  `idcompagnie` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `compagnie`
--

INSERT INTO `compagnie` (`idcompagnie`, `nom`) VALUES
(1, 'Air France'),
(2, 'Air Europa'),
(3, 'Air Italy'),
(4, 'Aigle Azur'),
(7, 'Air Algérie'),
(8, 'Air caraibes');

-- --------------------------------------------------------

--
-- Structure de la table `escale`
--

CREATE TABLE `escale` (
  `vol_idvol` int(11) NOT NULL,
  `aeroport_idaeroport` varchar(5) NOT NULL,
  `date_depart` datetime NOT NULL,
  `date_arrive` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `passager`
--

CREATE TABLE `passager` (
  `idpassager` int(11) NOT NULL,
  `idpersonne` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `idPays` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `pays`
--

INSERT INTO `pays` (`idPays`, `nom`) VALUES
(1, 'France'),
(2, 'Allemagne'),
(4, 'Etats-Unis'),
(5, 'Japon'),
(6, 'Royaume-Uni'),
(7, 'Algérie'),
(8, 'Italie'),
(9, 'Canada'),
(11, 'Algérie');

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `idpersonne` int(11) NOT NULL,
  `nom` varchar(70) NOT NULL,
  `prenom` varchar(70) NOT NULL,
  `dateNaiss` date NOT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `pays_idPays` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `personne`
--

INSERT INTO `personne` (`idpersonne`, `nom`, `prenom`, `dateNaiss`, `adresse`, `ville`, `zipcode`, `pays_idPays`) VALUES
(2, 'CAROLE', 'Luigi', '1998-07-09', '3 Place Andre Malraux', 'Villeneuve-la-Garenne', '92390', 1);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `idvol` int(11) NOT NULL,
  `client_idpersonne` int(11) NOT NULL,
  `confirmation` enum('Confirm','Pending','Cancel','Refuse') NOT NULL,
  `personne_idpersonne` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `vol`
--

CREATE TABLE `vol` (
  `idvol` int(11) NOT NULL,
  `place` smallint(6) NOT NULL DEFAULT 300,
  `intitule` varchar(50) DEFAULT NULL,
  `aeroport_depart` varchar(5) NOT NULL,
  `aeroport_arrive` varchar(5) NOT NULL,
  `date_depart` datetime NOT NULL,
  `date_arrive` datetime NOT NULL,
  `idcompagnie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vol`
--

INSERT INTO `vol` (`idvol`, `place`, `intitule`, `aeroport_depart`, `aeroport_arrive`, `date_depart`, `date_arrive`, `idcompagnie`) VALUES
(6, 1, 'LOL', 'CDG', 'ALG', '2021-06-18 00:00:00', '2021-06-19 00:00:00', 1),
(7, 3, 'Vendredi', 'ALG', 'ORY', '2021-04-26 00:00:00', '2021-04-27 00:00:00', 7);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `aeroport`
--
ALTER TABLE `aeroport`
  ADD PRIMARY KEY (`idaeroport`),
  ADD KEY `FK_aeroport_pays` (`pays`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`personne_idpersonne`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- Index pour la table `compagnie`
--
ALTER TABLE `compagnie`
  ADD PRIMARY KEY (`idcompagnie`);

--
-- Index pour la table `escale`
--
ALTER TABLE `escale`
  ADD PRIMARY KEY (`vol_idvol`,`aeroport_idaeroport`),
  ADD KEY `fk_escale_aeroport1_idx` (`aeroport_idaeroport`);

--
-- Index pour la table `passager`
--
ALTER TABLE `passager`
  ADD PRIMARY KEY (`idpassager`),
  ADD KEY `FK_passager_personne` (`idpersonne`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`idPays`);

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`idpersonne`),
  ADD KEY `fk_personne_pays1_idx` (`pays_idPays`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`idvol`,`client_idpersonne`),
  ADD KEY `FK_reservation_vol` (`idvol`),
  ADD KEY `fk_reservation_client1_idx` (`client_idpersonne`),
  ADD KEY `fk_reservation_personne1_idx` (`personne_idpersonne`);

--
-- Index pour la table `vol`
--
ALTER TABLE `vol`
  ADD PRIMARY KEY (`idvol`),
  ADD KEY `FK_vol_aeroport` (`aeroport_depart`),
  ADD KEY `FK_vol_aeroport_2` (`aeroport_arrive`),
  ADD KEY `FK_vol_compagnie` (`idcompagnie`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `compagnie`
--
ALTER TABLE `compagnie`
  MODIFY `idcompagnie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `passager`
--
ALTER TABLE `passager`
  MODIFY `idpassager` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `idPays` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `personne`
--
ALTER TABLE `personne`
  MODIFY `idpersonne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `vol`
--
ALTER TABLE `vol`
  MODIFY `idvol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `aeroport`
--
ALTER TABLE `aeroport`
  ADD CONSTRAINT `FK_aeroport_pays` FOREIGN KEY (`pays`) REFERENCES `pays` (`idPays`);

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `fk_client_personne1` FOREIGN KEY (`personne_idpersonne`) REFERENCES `personne` (`idpersonne`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `escale`
--
ALTER TABLE `escale`
  ADD CONSTRAINT `fk_escale_aeroport1` FOREIGN KEY (`aeroport_idaeroport`) REFERENCES `aeroport` (`idaeroport`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_escale_vol1` FOREIGN KEY (`vol_idvol`) REFERENCES `vol` (`idvol`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `passager`
--
ALTER TABLE `passager`
  ADD CONSTRAINT `FK_passager_personne` FOREIGN KEY (`idpersonne`) REFERENCES `personne` (`idpersonne`);

--
-- Contraintes pour la table `personne`
--
ALTER TABLE `personne`
  ADD CONSTRAINT `fk_personne_pays1` FOREIGN KEY (`pays_idPays`) REFERENCES `pays` (`idPays`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK_reservation_vol` FOREIGN KEY (`idvol`) REFERENCES `vol` (`idvol`),
  ADD CONSTRAINT `fk_reservation_client1` FOREIGN KEY (`client_idpersonne`) REFERENCES `client` (`personne_idpersonne`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reservation_personne1` FOREIGN KEY (`personne_idpersonne`) REFERENCES `personne` (`idpersonne`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `vol`
--
ALTER TABLE `vol`
  ADD CONSTRAINT `FK_vol_aeroport` FOREIGN KEY (`aeroport_depart`) REFERENCES `aeroport` (`idaeroport`),
  ADD CONSTRAINT `FK_vol_aeroport_2` FOREIGN KEY (`aeroport_arrive`) REFERENCES `aeroport` (`idaeroport`),
  ADD CONSTRAINT `FK_vol_compagnie` FOREIGN KEY (`idcompagnie`) REFERENCES `compagnie` (`idcompagnie`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
