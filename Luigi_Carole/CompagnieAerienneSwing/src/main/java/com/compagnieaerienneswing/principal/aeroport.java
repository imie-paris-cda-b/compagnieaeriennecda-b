/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compagnieaerienneswing.principal;

import java.sql.*;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author luigi
 */
public class aeroport extends javax.swing.JFrame {

    /**
     * Creates new form aeroport
     */
    public aeroport() {
        initComponents();
        this.setTitle("Air Babouche - Gestion des aéroports");
        setLocationRelativeTo(null);
        alimenterlist();
        tableUpdate();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNom = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtVille = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        AddAeroport = new javax.swing.JToggleButton();
        UpdateAeroport = new javax.swing.JToggleButton();
        DeleteAeroport = new javax.swing.JToggleButton();
        txtIDAeroport = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        boxPays = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableAeroport = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuClient = new javax.swing.JMenu();
        menuCompagnie = new javax.swing.JMenu();
        menuEscale = new javax.swing.JMenu();
        menuPassager = new javax.swing.JMenu();
        menuPays = new javax.swing.JMenu();
        menuPersonne = new javax.swing.JMenu();
        menuReservation = new javax.swing.JMenu();
        menuVol = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Gestion des aéroports");

        jLabel3.setText("Nom");

        jLabel4.setText("Ville");

        jLabel7.setText("Pays");

        AddAeroport.setText("Ajouter");
        AddAeroport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddAeroportActionPerformed(evt);
            }
        });

        UpdateAeroport.setText("Modifier");
        UpdateAeroport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdateAeroportActionPerformed(evt);
            }
        });

        DeleteAeroport.setText("Supprimer");
        DeleteAeroport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeleteAeroportActionPerformed(evt);
            }
        });

        jLabel2.setText("ID Aéroport");

        boxPays.setName(""); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(AddAeroport, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(106, 106, 106)
                                .addComponent(UpdateAeroport, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(DeleteAeroport, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtNom, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtIDAeroport, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(42, 42, 42)))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(boxPays, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtVille, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(51, 51, 51))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(126, 126, 126)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(56, 56, 56))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtVille, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2)
                    .addComponent(txtIDAeroport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel3)
                    .addComponent(txtNom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(boxPays, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UpdateAeroport)
                    .addComponent(DeleteAeroport)
                    .addComponent(AddAeroport))
                .addContainerGap())
        );

        tableAeroport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "IDAeroport", "Nom", "Ville", "Pays"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableAeroport.setPreferredSize(new java.awt.Dimension(1200, 80));
        jScrollPane1.setViewportView(tableAeroport);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 638, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );

        menuClient.setText("Client");
        menuClient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClientActionPerformed(evt);
            }
        });
        jMenuBar1.add(menuClient);

        menuCompagnie.setText("Compagnie");
        menuCompagnie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCompagnieActionPerformed(evt);
            }
        });
        jMenuBar1.add(menuCompagnie);

        menuEscale.setText("Escale");
        menuEscale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEscaleActionPerformed(evt);
            }
        });
        jMenuBar1.add(menuEscale);

        menuPassager.setText("Passager");
        menuPassager.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPassagerActionPerformed(evt);
            }
        });
        jMenuBar1.add(menuPassager);

        menuPays.setText("Pays");
        menuPays.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPaysActionPerformed(evt);
            }
        });
        jMenuBar1.add(menuPays);

        menuPersonne.setText("Personne");
        menuPersonne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPersonneActionPerformed(evt);
            }
        });
        jMenuBar1.add(menuPersonne);

        menuReservation.setText("Reservation");
        menuReservation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuReservationActionPerformed(evt);
            }
        });
        jMenuBar1.add(menuReservation);

        menuVol.setText("Vol");
        menuVol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuVolActionPerformed(evt);
            }
        });
        jMenuBar1.add(menuVol);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(109, 109, 109))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    Connection con;
    PreparedStatement pst;
    HashMap<String, Integer> listPays = new HashMap<String, Integer>();
    int cpt;
    
    private void AddAeroportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddAeroportActionPerformed
        // Ajoute un aeroport
        if(txtIDAeroport.getText().equals("") || txtNom.getText().equals("")
            || txtVille.getText().equals("")
            || boxPays.getSelectedItem().toString().equals("") ){
            javax.swing.JOptionPane.showMessageDialog(this,"Vous devez remplir tous les "
                + "champs \n","ATTENTION!",javax.swing.JOptionPane.INFORMATION_MESSAGE);
            txtNom.requestFocus();
        }else{
            // AJOUTER UN PRODUIT
            String ID = txtIDAeroport.getText();
            String Nom= txtNom.getText();
            String Ville= txtVille.getText();
            String Pays= boxPays.getSelectedItem().toString();

            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airbabouche", "root", "");
                pst=con.prepareStatement("INSERT INTO `aeroport` (idaeroport,nom,ville,pays) "
                    + "VALUES (?,?,?,?)");
                pst.setString(1, ID);
                pst.setString(2,Nom);
                pst.setString(3,Ville);
                pst.setString(4,Pays);

                pst.executeUpdate();
                JOptionPane.showMessageDialog(this, "Données enregistrées dans la DB");

                txtNom.setText("");
                txtIDAeroport.setText("");
                tableUpdate(); //Mettre à jour la table

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(aeroport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_AddAeroportActionPerformed

    public void alimenterlist(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airbabouche", "root", "");
            pst=con.prepareStatement("SELECT * FROM pays");
            ResultSet rs = pst.executeQuery();

            ResultSetMetaData rsmd=rs.getMetaData();  //Je récupère mes données
            cpt=rsmd.getColumnCount();    //Je recupère le nombre de colonnes dans la table
            while(rs.next()){
                listPays.put(rs.getString("nom"),rs.getInt("idPays"));
                boxPays.addItem(rs.getString("nom"));
            }
        }
        
        catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(compagnie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void tableUpdate(){
        try {
            //Mise à jour du tableau
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airbabouche", "root", "");
            pst=con.prepareStatement("SELECT * FROM aeroport");  
            ResultSet rs = pst.executeQuery();
            
            ResultSetMetaData rsmd=rs.getMetaData();  //Je récupère mes données
            cpt=rsmd.getColumnCount();    //Je recupère le nombre de colonnes dans la table
            DefaultTableModel  dtm=(DefaultTableModel)tableAeroport.getModel();
            dtm.setRowCount(0);
            
            while(rs.next()){
                
                Vector vect = new Vector();
                
                for(int i=1;i<=cpt;i++){
                    vect.add(rs.getString("idaeroport"));
                    vect.add(rs.getString("nom"));
                    vect.add(rs.getString("ville"));
                    vect.add(rs.getString("pays"));
                }
                dtm.addRow(vect);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(aeroport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void UpdateAeroportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UpdateAeroportActionPerformed
        // Met à jour la liste des aeroports.
    }//GEN-LAST:event_UpdateAeroportActionPerformed

    private void DeleteAeroportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeleteAeroportActionPerformed
        // Supprime un aeroport
        DefaultTableModel  dtm=(DefaultTableModel)tableAeroport.getModel();
        int selectedIndex = tableAeroport.getSelectedRow();
        String IDAeroport =dtm.getValueAt(selectedIndex, 0).toString();
        int dialogResult =JOptionPane.showConfirmDialog(null, "Voulez-vous supprimer cette"
            + "donnée ?","ATTENTION!",JOptionPane.YES_NO_OPTION);

        if(dialogResult==JOptionPane.YES_OPTION){
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                con = DriverManager.getConnection("jdbc:mysql://localhost:3306/airbabouche", "root", "");
                pst=con.prepareStatement("delete from aeroport where idaeroport=?");

                pst.setString(1, IDAeroport);
                pst.executeUpdate();
                JOptionPane.showMessageDialog(this, "Données supprimées dans la DB");

                tableUpdate(); //Mettre à jour la table

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(aeroport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_DeleteAeroportActionPerformed

    private void menuVolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuVolActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new vol().setVisible(true);

    }//GEN-LAST:event_menuVolActionPerformed

    private void menuClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClientActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new client().setVisible(true);
    }//GEN-LAST:event_menuClientActionPerformed

    private void menuCompagnieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCompagnieActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new compagnie().setVisible(true);
    }//GEN-LAST:event_menuCompagnieActionPerformed

    private void menuEscaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEscaleActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new escale().setVisible(true);
    }//GEN-LAST:event_menuEscaleActionPerformed

    private void menuPassagerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPassagerActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new escale().setVisible(true);
    }//GEN-LAST:event_menuPassagerActionPerformed

    private void menuPaysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPaysActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new pays().setVisible(true);
    }//GEN-LAST:event_menuPaysActionPerformed

    private void menuPersonneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPersonneActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new personne().setVisible(true);
    }//GEN-LAST:event_menuPersonneActionPerformed

    private void menuReservationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuReservationActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        new reservation().setVisible(true);
    }//GEN-LAST:event_menuReservationActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(aeroport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(aeroport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(aeroport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(aeroport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new aeroport().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton AddAeroport;
    private javax.swing.JToggleButton DeleteAeroport;
    private javax.swing.JToggleButton UpdateAeroport;
    private javax.swing.JComboBox<String> boxPays;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenu menuClient;
    private javax.swing.JMenu menuCompagnie;
    private javax.swing.JMenu menuEscale;
    private javax.swing.JMenu menuPassager;
    private javax.swing.JMenu menuPays;
    private javax.swing.JMenu menuPersonne;
    private javax.swing.JMenu menuReservation;
    private javax.swing.JMenu menuVol;
    private javax.swing.JTable tableAeroport;
    private javax.swing.JTextField txtIDAeroport;
    private javax.swing.JTextField txtNom;
    private javax.swing.JTextField txtVille;
    // End of variables declaration//GEN-END:variables
}
