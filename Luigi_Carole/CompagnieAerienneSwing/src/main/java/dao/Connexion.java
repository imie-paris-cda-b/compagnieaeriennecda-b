/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.*;
import java.sql.DriverManager;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author luigi
 */        
public class Connexion {
    private String db = "jdbc:mysql://localhost:3306/airbabouchedb";
    private String user = "root";
    private String password = "";
    Connection con;
    
    public Connection initConnexion(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(db, user, password);
            System.out.println("Connection réussie");
            }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("Erreur");
            System.exit(0);
        }
        return con;
    }
}
