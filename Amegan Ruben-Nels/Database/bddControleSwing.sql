Drop database if exists baboucheAirline;
create database baboucheAirline;
	use baboucheAirline;

create table pays(
	idPays int(5) not null auto_increment,
	nom varchar(50) not null,
	primary key(idPays)
);

create table aeroport(
	idAeroport int(5) not null auto_increment,
	nomAeroport varchar(50) not null,
	ville varchar(50) not null,
	idPays int(5) not null,
	primary key(idAeroport),
	foreign key(idPays) references pays(idPays)
);

create table personne(
	idPersonne int(5) not null auto_increment,
	nomPersonne varchar(50) not null,
	prenom varchar(50) not null,
	dateNaiss varchar(50) not null,
	adresse  varchar(50) not null,
	villePersonne varchar(50) not null,
	zipCode varchar(20) not null,
	idPays int(5) not null,
	primary key(idPersonne),
	foreign key(idPays) references pays(idPays)
);

create table client(
	idPersonne int(5) not null,
	email varchar(150) not null,
	pass varchar(80) not null,
	primary key(idPersonne),
	foreign key(idPersonne) references personne(idPersonne)
);

create table compagnie(
	idCompagnie int(5) not null auto_increment,
	nomCompagnie varchar(50) not null,
	primary key(idCompagnie)
);

create table vol(
	idVol int(5) not null auto_increment,
	place int(5) not null,
	libelle varchar(50) not null,
	dateDepart varchar(50) not null,
	dateArrive varchar(50) not null,
	aeroportDepart varchar(50) not null,
	aeroportArrive varchar(50) not null,
	idCompagnie int(5) not null,
	primary key(idVol),
	foreign key(idCompagnie) references compagnie(idCompagnie)
);

create table escale(
	idVol int(5) not null,
	idAeroport int(5) not null,
	date_Depart varchar(50) not null,
	date_Arrive varchar(50) not null,
	primary key(idVol,idAeroport),
	foreign key(idVol) references vol(idVol),
	foreign key(idAeroport) references aeroport(idAeroport)
);

create table passager(
	idPassager int(5) not null auto_increment,
	idPersonne int(5) not null,
	primary key(idPassager),
	foreign key(idPersonne) references personne(idPersonne)
);

create table reservation(
	idVol int(5) not null,
	idPersonne int(5) not null,
	confirmation ENUM(
		'Confirm',
		'Pending',
		'Cancel',
		'Refuse') not null,
	client_idPersonne int(5) not null,
	primary key(idVol,client_idPersonne),
	foreign key(idVol) references vol(idVol),
	foreign key(idPersonne) references personne(idPersonne),
	foreign key(client_idPersonne) references client(idPersonne)
);